export class Global {
    private _urlServer: String = 'http://localhost:3000/';

    constructor() {
    }

    public get urlOfficer(): any {
        return  `${this._urlServer}OurDepartment/`;
    }

    public get urlApiModel(): any {
        return  `${this._urlServer}modelName/`;
    }

    public get urlServer(): any {
        return  this._urlServer;
    }

}
