import { OurDepartmentComponent } from './Components/OurDepartment/OurDepartment.component';
import { TempleteComponent } from './Components/TempleteComponent/Templete.component';
import { NgModule } from '@angular/core';


import { Routes, RouterModule } from '@angular/router';
// import { AuthGuard } from './shared/Guard/Auth';
const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: '/templeteroute' },
    {path: 'templeteroute', component: TempleteComponent},
    { path: 'OurDepartment', component: OurDepartmentComponent },
    // { path: 'login', loadChildren: './components/Login/Login.module#LoginModule' },
//     { path: 'api/verify/:token', loadChildren: './components/ResetPassword/ResetPassword.module#ResetPasswordModule' },
//    { path: 'showIdeas', loadChildren: './components/ShowIdeas/ShowIdeas.module#ShowIdeasModule',canActivate:[AuthGuard]}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
