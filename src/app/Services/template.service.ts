import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Global } from '../Shared/Global';
import { TempleteModel } from '../Models/TempleteModel';

@Injectable()
// change the names whit your own criteria and teh correct nomenclature
export class TemplateService {
    private global: Global;
    constructor(private http: HttpClient) {
        this.global = new Global();
    }

    private generateHeaders(token): HttpHeaders {
        const headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        headers.set('Authorization', token);

        return headers;
    }

    public Create(token, model: TempleteModel): Observable<any> {
        return this.http.post(`${this.global.urlApiModel}`, model,
            { headers: this.generateHeaders(token) });
    }

    public GetModel(token): Observable<any> {
        return this.http.get(`${this.global.urlApiModel}`,
            { headers: this.generateHeaders(token) });
    }

    public GetModels(token): Observable<any> {
        return this.http.get(`${this.global.urlApiModel}/models`,
            { headers: this.generateHeaders(token) });
    }

    public UpdateModel(token, model: TempleteModel, id: string): Observable<any> {
        return this.http.put(`${this.global.urlApiModel}/${id}`, model,
            { headers: this.generateHeaders(token) });
    }

    public DeleteModel(token, id: string): Observable<any> {
        return this.http.delete(`${this.global.urlApiModel}/${id}`,
            { headers: this.generateHeaders(token) });
    }
}
