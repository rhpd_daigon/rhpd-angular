
import { Global } from './../Shared/Global';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
@Injectable()
// change the names whit your own criteria and teh correct nomenclature
export class UploadService {
    private global: Global;
    constructor(private http: HttpClient) {
        this.global = new Global();
    }

  
  makeFileRequest(params: Array<string>, files: Array<File>, token: string, name: string, id, image: string) {

console.log("image file")
console.log(files)
    return new Promise(function (resolve, reject) {
      const formData: FormData = new FormData();
      const xhr = new XMLHttpRequest();

      for (var i = 0; i < files.length; i++) {
        formData.append(name, files[i], files[i].name);
        console.log(formData);
      }

      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status === 200) {
            resolve(JSON.parse(xhr.response));
          }
          else {
            reject(xhr.response);
          }
        }
      }

      xhr.open('POST', `${this.global.urlOfficer}image/${id}`, true);
      xhr.setRequestHeader('Authorization', token);
      xhr.setRequestHeader('oldimage', image);
      xhr.send(formData);
    });
  }

}