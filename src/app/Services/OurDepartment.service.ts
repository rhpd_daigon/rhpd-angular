import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Global } from '../Shared/Global';
import { police_officer } from '../Models/police_officer';

@Injectable()
// change the names whit your own criteria and teh correct nomenclature
export class OurDepartmentService {
    private global: Global;
    constructor(private http: HttpClient) {
        this.global = new Global();
    }

    private generateHeaders(token): HttpHeaders {
        const headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        headers.set('Authorization', token);

        return headers;
    }

    public createOfficer(token, officer: police_officer): Observable<any> {
        return this.http.post(`${this.global.urlOfficer}`, officer,
            { headers: this.generateHeaders(token) });
    }

    public uploadImage(params: Array<string>, files: Array<File>, token: string, name: string, id, image: string, id_rank:number) {

        console.log("image file");
        console.log(files);

        const formData: FormData = new FormData();

        for (var i = 0; i < files.length; i++) {
            formData.append(name, files[i], files[i].name);
            console.log(formData.getAll(name));
        }
        const headers = this.generateHeaders(token);
        headers.set('oldimage', image);
        return this.http.post(`${this.global.urlOfficer}image/${id}/${id_rank}`, formData,
            { headers: headers });

    }
    public GetOfficer(token, id: number): Observable<any> {
        return this.http.get(`${this.global.urlOfficer}one/${id}`,
            { headers: this.generateHeaders(token) });
    }

    public GetOfficersByRank(token, rank: number): Observable<any> {
        return this.http.get(`${this.global.urlOfficer}officers/${rank}`,
            { headers: this.generateHeaders(token) });
    }

    public GetFile(token, file: string): Observable<any> {
        return this.http.get(`${this.global.urlOfficer}${file}`,
            { headers: this.generateHeaders(token) });
    }

    public UpdateOfficer(token, police_officer: police_officer, id: string, rank:number): Observable<any> {
        return this.http.put(`${this.global.urlOfficer}update/${id}/${rank}`, police_officer,
            { headers: this.generateHeaders(token) });
    }

    public DeleteOfficer(token, id: string, rank:number): Observable<any> {
        console.log(id,rank,"aqui" )
        return this.http.delete(`${this.global.urlOfficer}del/${id}/${rank}`,
            { headers: this.generateHeaders(token) });
    }
}
