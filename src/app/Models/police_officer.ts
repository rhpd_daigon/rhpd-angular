export class police_officer {
    id: number ;
    name: string;
    id_rank: number;
    photo: string;
    division: string;
    bio: string;
    fallen: boolean;
    email:string;

}