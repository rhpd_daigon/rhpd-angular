import { Route, Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import swal from 'sweetalert2';
import { TemplateService } from '../../Services/template.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';



@Component({
  selector: 'app-templete',
  templateUrl: './Templete.component.html',
  styleUrls: ['./Templete.component.css'],
  providers: [TemplateService],
  animations: [
    trigger('visibility', [
      state('inactive', style({
        backgroundColor: '#eee',
        transform: 'scale(1)',
        display: 'none'
      })),
      state('active',   style({
        backgroundColor: '#d9d2c6',
        transform: 'scale(1.1)',
        display: 'block',
      })),
      transition('inactive => active', animate('100ms ease-in')),
      transition('active => inactive', animate('100ms ease-out'))
    ])
  ]
})

export class TempleteComponent implements OnInit {
  public isCollapsed = false;
  @Input() moreText;
  status: string = 'inactive';
  PNotify:any;
  constructor(
    private router: Router,
    // private route: Route,
    private _Service: TemplateService,
    private modalService: NgbModal,

  ) {
  }

  ngOnInit() {
    // PNotify.notice({
    //   title: 'Regular Notice',
    //   text: 'Check me out! I\'m a notice.'
    // });

    // swal('Oops...', 'Something went wrong!', 'error')
    // new this.PNotify({
    //   title: 'Regular Notice',
    //   text: 'Check me out! I\'m a notice.'
    // });
  }
  open(content) {
    this.modalService.open(content)
  }


  public Create() {}

  public update() {}

  public delete() {}

  public getOne(id: string) {}

  public getAll() {}
  changeVisibility() {
    this.status = this.status === 'inactive' ? 'active' : 'inactive';
  }

}
