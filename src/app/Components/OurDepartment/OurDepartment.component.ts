import { Global } from './../../Shared/Global';
import { OurDepartmentService } from './../../Services/OurDepartment.service';
import { UploadService } from './../../services/upload.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { police_officer } from './../../Models/police_officer';
import { Route, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import swal from 'sweetalert2';


@Component({
  selector: 'app-OurDepartment',
  templateUrl: './OurDepartment.component.html',
  styleUrls: ['./OurDepartment.component.css'],
  providers: [OurDepartmentService, UploadService]
})


export class OurDepartmentComponent implements OnInit {
  public officer: police_officer;
  public imagePath: string;
  public captains: Array<any> = [];
  public officers: Array<any> = [];
  public isCollapsed = true;
  public imageName;
  public avatar;
  private global: Global;
  public oldphoto:string;


  constructor(


    private router: Router,
    private _Service: OurDepartmentService,
    private _uploadService: UploadService,
    private modalService: NgbModal,

  ) {
    this.global = new Global();
    this.officer = new police_officer;
    this.getAllCaptains();
    this.getAllOfficers();
    this.imagePath = this.global.urlOfficer;

  }

  ngOnInit() {
  }

  //create officer
  public CreateOfficer() {
    try {
  
      this._Service.createOfficer(null, this.officer).subscribe(
        res => {
          if (this.fileToUpload[0] !== null) {

            this._Service.uploadImage([], this.fileToUpload, null, 'image', res.id, res.photo, res.id_rank)
              .subscribe(
                (res: any) => {

               
                  if (this.officer.id_rank == 1)
                    this.officers = res.police_officers;

                  if (this.officer.id_rank == 2)
                    this.captains = res.police_officers;

          
                  //localStorage.setItem('identity', JSON.stringify(this.user));
                  //const image_path = GLOBAL.urlGetUserImage + this.user.image;
                  //document.getElementById('image-logged').setAttribute('src', image_path);
                  this.officer = new police_officer();
                  swal(
                    'Bien hecho',
                    'El usuario se ha actualizado',
                    'success'
                  )




                },
                (err) => {
                  // try {
                  //   var error = JSON.parse(err._body);

                  //   if(error['message'] !== undefined){
                  //       swal("Error", error.message, "error")
                  //   }
                  //   else if (error['isCorrect'] !== null) {
                  //     console.log("here")
                  //     if (!error.isCorrect) {

                  //       swal({title:"Sesión expirada", text:"Tu sesión a expirado, debes de volver a iniciar sesión", type:"info",allowOutsideClick: false})
                  //         .then(() => {
                  //           localStorage.clear();
                  //           this._router.navigate(['/login']);
                  //           window.location.reload()
                  //         });


                  //     }
                  //   }
                  // } catch (error) {
                  //   swal("Error", "Ha ocurrido un error en el servidor vuelva a intentarlo mas tarde.", "error");
                  // }
                }
              );

          }
          else {
            this.officer = new police_officer();
            swal(
              'Bien hecho',
              'El usuario se ha actualizado',
              'success'
            ).then(() => {
              window.location.reload();
            });


          }

        },
        err => {

        }
      );
    } catch (error) {
      console.log(error);

    }
  }
  //change
  public fileToUpload: Array<File> = [];

  filechangeEvent(fileInut: any) {
  
    if (fileInut.target.files.length > 0) {
      this.fileToUpload = <Array<File>>fileInut.target.files;
      this.imageName = this.fileToUpload[0].name;

      let reader = new FileReader();

      reader.onload = (e: any) => {

        this.avatar = e.target.result;
     
      }

      reader.readAsDataURL(this.fileToUpload[0]);
    }


  }
  //update offcer
  public updateOfficer(id,rank) {
    this.oldphoto = this.officer.photo 
    this._Service.UpdateOfficer(null, this.officer, id,rank).subscribe(
      res => {
        console.log("res")
      console.log(res)

        if (this.fileToUpload[0] !== null) {
          

          this._Service.uploadImage([], this.fileToUpload, null, 'image', id, this.oldphoto, rank)
            .subscribe(
              (res: any) => {

                console.log("res")
                console.log(res)
                  if (rank == 1)
                    this.officers = res.police_officers;

                  if (rank  == 2)
                    this.captains = res.police_officers;

                this.officer = new police_officer();
                swal(
                  'Bien hecho',
                  'El usuario se ha actualizado',
                  'success'
                )
              },
              (err) => {
             
              }
            );

        }
      

        //localStorage.setItem('identity', JSON.stringify(this.user));
        //const image_path = GLOBAL.urlGetUserImage + this.user.image;
        //document.getElementById('image-logged').setAttribute('src', image_path);
        this.officer = new police_officer();
        swal(
          'Bien hecho',
          'El usuario se ha actualizado',
          'success'
        )
      },
      err => {

      }
    )
  }
  //delete offcer
  public delete(id,rank) {

    swal({
      title: '¿Are you sure?',
      text: "Deleted officers can not be recovered",
      showCancelButton: true,
      confirmButtonClass: 'btn btnAdd',
      cancelButtonClass: 'btn btn-light',
      confirmButtonText: 'OK',
      // imageUrl: 'assets/icons/waring.png',
      imageAlt: 'waring'
    })
    .then(deleteOfficer =>{

      console.log(deleteOfficer)

      if(deleteOfficer.value ==true && deleteOfficer.dismiss !== "cancel" ){

        this._Service.DeleteOfficer(null,id,rank).subscribe(
          res=>{

      
            swal(
              'Deleted',
              'Deleted successfully!',
              'success'
            )
         
            if (rank == 1)
            this.officers = res.police_officers;
    
          if (rank  == 2)
            this.captains = res.police_officers;
          },
        err=>{
          console.log(err);
        }
        )

      }
    }
    )
   }
  //get one officer
  public getOfficer(id: number) {
    this._Service.GetOfficer(null, id).subscribe(
      res => {
        this.officer = new police_officer;
        this.officer = res;
      },
      err => {
        console.log(err)
      }
    )
  }
  //get all officers
  public getAllOfficers() {
    try {

      this._Service.GetOfficersByRank(null, 1).subscribe(res => {
        this.officers = null;
        this.officers = res.police_officers;

      })

    } catch (error) {

    }
  }
  //get all captains
  public getAllCaptains() {
    try {

      this._Service.GetOfficersByRank(null, 2).subscribe(res => {
        this.captains = null;
        this.captains = res.police_officers;

      })

    } catch (error) {

    }
  }

  public modalAddOfficer(content) {
    this.officer = new police_officer;
    this.avatar = null;
    console.log(this.officer)
    this.modalService.open(content)
  }
  public modalEditOfficer(content, id) {
    this.officer =new police_officer;
    this.avatar = null;
    this.getOfficer(id);
    this.modalService.open(content);
  }
  public modalEdittitle(content) {
    this.modalService.open(content)
  }

  public rank(arg) {
    if (arg.target.value == 1) {
      this.officer.id_rank = 1;
    }
    if (arg.target.value == 2) {
      this.officer.id_rank = 2;
    }

  }










}
